<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel;

use Drupal\physical\Weight;
use Drupal\commerce_price\Price;
use Drupal\profile\Entity\Profile;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\Entity\ShippingMethod;
use Drupal\Tests\commerce_shipping\Kernel\ShippingKernelTestBase;

/**
 * Provides a base class for DHL Express kernel tests.
 */
abstract class DhlExpressKernelTestBase extends ShippingKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'profile',
    'physical',
    'commerce_order',
    'commerce_shipping',
    'commerce_dhl_express',
    'commerce_price',
  ];

  /**
   * A shipment.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $user = $this->createUser(['mail' => $this->randomString() . '@example.com']);

    $order_item = OrderItem::create([
      'type' => 'test',
      'title' => 'T-shirt (red, large)',
      'quantity' => 2,
      'unit_price' => new Price('30.00', 'USD'),
    ]);
    $order_item->save();

    $order = Order::create([
      'type' => 'default',
      'state' => 'fulfillment',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
    ]);
    $order->save();

    $shipment = Shipment::create([
      'type' => 'default',
      'state' => 'ready',
      'order_id' => $order->id(),
      'title' => 'Shipment',
      'amount' => new Price('12.00', 'USD'),
    ]);
    $shipment->save();

    $shipping_method = ShippingMethod::create([
      'name' => $this->randomString(),
      'plugin' => [
        'target_plugin_id' => 'dhl_express',
        'target_plugin_configuration' => [
          'mode' => 'test',
          'api_information' => [
            'credentials' => [
              'account' => $this->randomString(),
              'username' => $this->randomString(),
              'password' => $this->randomString(),
            ],
            'requested_shipment' => [
              'ship' => [
                'shipper' => [
                  'contact' => [
                    'person_name' => 'Léon Blum',
                    'company_name' => 'Blum&Co',
                    'phone_number' => '0987654321',
                    'email_address' => 'leon@gmail.com',
                  ],
                  'address' => [
                    'country_code' => 'FR',
                    'locality' => 'Paris',
                    'postal_code' => '75002',
                    'address_line1' => '38 Rue du Sentier',
                    'address_line2' => 'Le Marteley',
                  ],
                ],
              ],
              'international_detail' => [
                'commodities' => [
                  'description' => 'Clothes',
                ],
              ],
            ],
          ],
          'options' => [
            'tracking_url' => 'https://www.dhl.com/global-en/home/tracking.html?tracking-id=[tracking_code]',
          ],
          'rating' => [
            'mode' => 'flat',
            'options' => [
              'flat' => [
                'rate_amount' => [
                  'number' => '10',
                  'currency_code' => 'EUR',
                ],
              ],
            ],
          ],
        ],
      ],
      'status' => 1,
    ]);
    $shipping_method->save();
    $this->shippingMethod = $shipping_method;
    $shipment->setShippingMethod($shipping_method);

    $package_type_manager = $this->container->get('plugin.manager.commerce_package_type');
    $package_type = $package_type_manager->createInstance('custom_box');
    $shipment->setPackageType($package_type);

    $shipment->setShippingService('N');

    $profile = Profile::create([
      'type' => 'customer',
      'address' => [
        'country_code' => 'FR',
        'locality' => 'Paris',
        'postal_code' => '75002',
        'address_line1' => '38 Rue du Sentier',
        'address_line2' => 'Le Marteley',
        'given_name' => 'Léon',
        'family_name' => 'Blum',
        'organization' => 'Blum&Co',
      ],
    ]);
    $profile->save();
    $this->shippingProfile = $profile;

    $shipment->setShippingProfile($profile);

    $shipment->setTitle('Shipment #1');

    $shipment_item = new ShipmentItem([
      'order_item_id' => 1,
      'title' => 'T-shirt (red, large)',
      'quantity' => 2,
      'weight' => new Weight('40', 'kg'),
      'declared_value' => new Price('30.00', 'USD'),
    ]);

    $shipment->setItems([$shipment_item]);

    $shipment->setTrackingCode('0123456789');

    $this->shipment = $shipment;
  }

}
