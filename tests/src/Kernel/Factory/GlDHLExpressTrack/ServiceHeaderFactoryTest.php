<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\ServiceHeader;

/**
 * Tests the ServiceHeader Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack\ServiceHeaderFactory
 * @group commerce_dhl_express
 */
final class ServiceHeaderFactoryTest extends DhlExpressKernelTestBase {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The time.
   *
   * @var \Drupal\Core\Datetime\Time
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->dateFormatter = $this->container->get('date.formatter');
    $this->time = $this->container->get('datetime.time');
  }

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $serviceHeader = ServiceHeaderFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(ServiceHeader::class, $serviceHeader);

    /** @var \Drupal\commerce_dhl_express\Plugin\Commerce\ShippingMethod\DhlExpressInterface $shipping_method_plugin */
    $shipping_method_plugin = $this->shipment->getShippingMethod()->getPlugIn();

    $this->assertEquals(
      $this->dateFormatter->format(
        $this->time->getCurrentTime(),
        'custom',
        'Y-m-d\TH:i:sP'
      ),
      $serviceHeader->getMessageTime()
    );
    $this->assertEquals($shipping_method_plugin::WEBSTORE_PLATFORM, $serviceHeader->getWebstorePlatform());
    $this->assertEquals($shipping_method_plugin::SHIPPING_SYSTEM_PLATFORM, $serviceHeader->getShippingSystemPlatform());
    $this->assertEquals($shipping_method_plugin::PLUG_IN, $serviceHeader->getPlugIn());
  }

}
