<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\PubTrackingRequest;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackShipmentRequest;

/**
 * Tests the TrackShipmentRequest Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack\TrackShipmentRequestFactory
 * @group commerce_dhl_express
 */
final class TrackShipmentRequestFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $trackShipmentRequest = TrackShipmentRequestFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(TrackShipmentRequest::class, $trackShipmentRequest);

    $this->assertInstanceOf(PubTrackingRequest::class, $trackShipmentRequest->getTrackingRequest());
  }

}
