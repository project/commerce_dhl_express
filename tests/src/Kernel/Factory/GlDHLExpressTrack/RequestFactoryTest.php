<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\Request;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\ServiceHeader;

/**
 * Tests the Request Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack\RequestFactory
 * @group commerce_dhl_express
 */
final class RequestFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $request = RequestFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(Request::class, $request);

    $this->assertInstanceOf(ServiceHeader::class, $request->getServiceHeader());
  }

}
