<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Maetva\DhlExpress\GlDHLExpressTrack\EnumType\LevelOfDetails;
use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\Request;
use Maetva\DhlExpress\GlDHLExpressTrack\ArrayType\ArrayOfAWBNumber;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackingRequest;

/**
 * Tests the TrackingRequest Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack\TrackingRequestFactory
 * @group commerce_dhl_express
 */
final class TrackingRequestFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $trackingRequest = TrackingRequestFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(TrackingRequest::class, $trackingRequest);

    $this->assertInstanceOf(ArrayOfAWBNumber::class, $trackingRequest->getAWBNumber());
    $this->assertEquals(LevelOfDetails::VALUE_ALL_CHECKPOINTS, $trackingRequest->getLevelOfDetails());
    $this->assertInstanceOf(Request::class, $trackingRequest->getRequest());
  }

}
