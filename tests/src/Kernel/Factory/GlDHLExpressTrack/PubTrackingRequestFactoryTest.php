<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackingRequest;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\PubTrackingRequest;

/**
 * Tests the PubTrackingRequest Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack\PubTrackingRequestFactory
 * @group commerce_dhl_express
 */
final class PubTrackingRequestFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $pubTrackingRequest = PubTrackingRequestFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(PubTrackingRequest::class, $pubTrackingRequest);

    $this->assertInstanceOf(TrackingRequest::class, $pubTrackingRequest->getTrackingRequest());
  }

}
