<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\GlDHLExpressTrack\ArrayType\ArrayOfAWBNumber;

/**
 * Tests the ArrayOfAWBNumber Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack\ArrayOfAWBNumberFactory
 * @group commerce_dhl_express
 */
final class ArrayOfAWBNumberFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $arrayOfAWBNumber = ArrayOfAWBNumberFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(ArrayOfAWBNumber::class, $arrayOfAWBNumber);
  }

}
