<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_AddressTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType;

/**
 * Tests the DocTypeRef_AddressType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_AddressTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_AddressTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->countryManager = $this->container->get('country_manager');
  }

  /**
   * @covers ::createRecipientAddressFromShipment
   */
  public function testCreateRecipientAddressFromShipment(): void {
    $docTypeRef_AddressType = DocTypeRef_AddressTypeFactory::createRecipientAddressFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_AddressType::class, $docTypeRef_AddressType);

    $address = $this->shipment->getShippingProfile()->get('address')->first()->getValue();
    $country_list = $this->countryManager->getList();
    $this->assertEquals(
      $address['address_line1'],
      $docTypeRef_AddressType->getStreetLines()
    );
    $this->assertEquals(
      $address['address_line2'],
      $docTypeRef_AddressType->getStreetLines2()
    );
    $this->assertEquals($address['locality'], $docTypeRef_AddressType->getCity());
    $this->assertEquals($address['postal_code'], $docTypeRef_AddressType->getPostalCode());
    $this->assertEquals($address['country_code'], $docTypeRef_AddressType->getCountryCode());
    $this->assertEquals($country_list[$address['country_code']]->render(), $docTypeRef_AddressType->getCountryName());
  }

  /**
   * @covers ::createShipperAddressFromShipment
   */
  public function testCreateShipperAddressFromShipment(): void {
    $docTypeRef_AddressType = DocTypeRef_AddressTypeFactory::createShipperAddressFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_AddressType::class, $docTypeRef_AddressType);

    $shipping_method_config = $this->shippingMethod->getPlugIn()->getConfiguration();
    $address = $shipping_method_config['api_information']['requested_shipment']['ship']['shipper']['address'];
    $country_list = $this->countryManager->getList();
    $this->assertEquals(
      $address['address_line1'],
      $docTypeRef_AddressType->getStreetLines()
    );
    $this->assertEquals(
      $address['address_line2'],
      $docTypeRef_AddressType->getStreetLines2()
    );
    $this->assertEquals($address['locality'], $docTypeRef_AddressType->getCity());
    $this->assertEquals($address['postal_code'], $docTypeRef_AddressType->getPostalCode());
    $this->assertEquals($address['country_code'], $docTypeRef_AddressType->getCountryCode());
    $this->assertEquals($country_list[$address['country_code']]->render(), $docTypeRef_AddressType->getCountryName());
  }

}
