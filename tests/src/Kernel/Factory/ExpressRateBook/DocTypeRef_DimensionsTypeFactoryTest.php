<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\physical\LengthUnit;
use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_DimensionsTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_DimensionsType;

/**
 * Tests the DocTypeRef_DimensionsType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_DimensionsTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_DimensionsTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_DimensionsType = DocTypeRef_DimensionsTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_DimensionsType::class, $docTypeRef_DimensionsType);

    $package = $this->shipment->getPackageType();
    $this->assertEquals(
      $package->getLength()->convert(LengthUnit::METER)->getNumber(),
      $docTypeRef_DimensionsType->getLength()
    );
    $this->assertEquals(
      $package->getWidth()->convert(LengthUnit::METER)->getNumber(),
      $docTypeRef_DimensionsType->getWidth()
    );
    $this->assertEquals(
      $package->getHeight()->convert(LengthUnit::METER)->getNumber(),
      $docTypeRef_DimensionsType->getHeight()
    );
  }

}
