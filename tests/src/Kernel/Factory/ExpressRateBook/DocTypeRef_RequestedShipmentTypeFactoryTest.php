<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\EnumType\PaymentInfo;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_PackagesType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipmentInfoType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_InternationDetailType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedShipmentType;

/**
 * Tests the DocTypeRef_RequestedShipmentType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RequestedShipmentTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_RequestedShipmentTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The time.
   *
   * @var \Drupal\Core\Datetime\Time
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->dateFormatter = $this->container->get('date.formatter');
    $this->time = $this->container->get('datetime.time');
  }

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_RequestedShipmentType = DocTypeRef_RequestedShipmentTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_RequestedShipmentType::class, $docTypeRef_RequestedShipmentType);

    $this->assertEquals(
      $this->dateFormatter->format(
        $this->time->getCurrentTime() + 60,
        'custom',
        'Y-m-d\TH:i:s\G\M\TP'
      ),
      $docTypeRef_RequestedShipmentType->getShipTimestamp()
    );
    $this->assertEquals(PaymentInfo::VALUE_DAP, $docTypeRef_RequestedShipmentType->getPaymentInfo());
    $this->assertInstanceOf(DocTypeRef_ShipmentInfoType::class, $docTypeRef_RequestedShipmentType->getShipmentInfo());
    $this->assertInstanceOf(DocTypeRef_InternationDetailType::class, $docTypeRef_RequestedShipmentType->getInternationalDetail());
    $this->assertInstanceOf(DocTypeRef_ShipType::class, $docTypeRef_RequestedShipmentType->getShip());
    $this->assertInstanceOf(DocTypeRef_PackagesType::class, $docTypeRef_RequestedShipmentType->getPackages());
  }

}
