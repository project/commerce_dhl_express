<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\physical\LengthUnit;
use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_DimensionsType2Factory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_DimensionsType2;

/**
 * Tests the DocTypeRef_DimensionsType2 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_DimensionsType2Factory
 * @group commerce_dhl_express
 */
final class DocTypeRef_DimensionsType2FactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_DimensionsType2 = DocTypeRef_DimensionsType2Factory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_DimensionsType2::class, $docTypeRef_DimensionsType2);

    $package = $this->shipment->getPackageType();
    $this->assertEquals(
      $package->getLength()->convert(LengthUnit::METER)->getNumber(),
      $docTypeRef_DimensionsType2->getLength()
    );
    $this->assertEquals(
      $package->getWidth()->convert(LengthUnit::METER)->getNumber(),
      $docTypeRef_DimensionsType2->getWidth()
    );
    $this->assertEquals(
      $package->getHeight()->convert(LengthUnit::METER)->getNumber(),
      $docTypeRef_DimensionsType2->getHeight()
    );
  }

}
