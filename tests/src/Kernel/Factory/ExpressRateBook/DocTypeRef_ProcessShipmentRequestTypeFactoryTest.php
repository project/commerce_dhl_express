<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\Request;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedShipmentType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ProcessShipmentRequestType;

/**
 * Tests the DocTypeRef_ProcessShipmentRequestType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ProcessShipmentRequestTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_ProcessShipmentRequestTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_ProcessShipmentRequestType = DocTypeRef_ProcessShipmentRequestTypeFactory::createFromShipment($this->shipment);

    $this->assertInstanceOf(DocTypeRef_ProcessShipmentRequestType::class, $docTypeRef_ProcessShipmentRequestType);
    $this->assertInstanceOf(Request::class, $docTypeRef_ProcessShipmentRequestType->getRequest());
    $this->assertInstanceOf(DocTypeRef_RequestedShipmentType::class, $docTypeRef_ProcessShipmentRequestType->getRequestedShipment());
  }

}
