<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_PackagesType2Factory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_PackagesType2;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedPackagesType2;

/**
 * Tests the DocTypeRef_PackagesType2 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_PackagesType2Factory
 * @group commerce_dhl_express
 */
final class DocTypeRef_PackagesType2FactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_PackagesType2 = DocTypeRef_PackagesType2Factory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_PackagesType2::class, $docTypeRef_PackagesType2);

    $this->assertInstanceOf(DocTypeRef_RequestedPackagesType2::class, $docTypeRef_PackagesType2->getRequestedPackages());
  }

}
