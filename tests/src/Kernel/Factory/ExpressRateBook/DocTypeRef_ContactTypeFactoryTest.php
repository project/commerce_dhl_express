<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ContactTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactType;

/**
 * Tests the DocTypeRef_ContactType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ContactTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_ContactTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createShipperContactFromShipment
   */
  public function testCreateShipperContactFromShipment(): void {
    $docTypeRef_ContactType = DocTypeRef_ContactTypeFactory::createShipperContactFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_ContactType::class, $docTypeRef_ContactType);

    $shipping_method_config = $this->shipment->getShippingMethod()->getPlugIn()->getConfiguration();
    $shipper_contact_config = $shipping_method_config['api_information']['requested_shipment']['ship']['shipper']['contact'];
    $this->assertEquals($shipper_contact_config['person_name'], $docTypeRef_ContactType->getPersonName());
    $this->assertEquals($shipper_contact_config['company_name'], $docTypeRef_ContactType->getCompanyName());
    $this->assertEquals($shipper_contact_config['phone_number'], $docTypeRef_ContactType->getPhoneNumber());
    $this->assertEquals($shipper_contact_config['email_address'], $docTypeRef_ContactType->getEmailAddress());
  }

  /**
   * @covers ::createRecipientContactFromShipment
   */
  public function testCreateRecipientContactFromShipment(): void {
    $docTypeRef_ContactType = DocTypeRef_ContactTypeFactory::createRecipientContactFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_ContactType::class, $docTypeRef_ContactType);

    $recipient_address = $this->shipment->getShippingProfile()->get('address')->first()->getValue();
    $recipient_name = $recipient_address['given_name'] . ' ' . $recipient_address['family_name'];
    $recipient_company = $recipient_address['organization'];
    $this->assertEquals($recipient_name, $docTypeRef_ContactType->getPersonName());
    $this->assertEquals(
      $recipient_company ? $recipient_company : $recipient_name,
      $docTypeRef_ContactType->getCompanyName()
    );
    $this->assertEquals('0000000000', $docTypeRef_ContactType->getPhoneNumber());
    $this->assertEquals(
      $this->shipment->getOrder()->getEmail(),
      $docTypeRef_ContactType->getEmailAddress()
    );
  }

}
