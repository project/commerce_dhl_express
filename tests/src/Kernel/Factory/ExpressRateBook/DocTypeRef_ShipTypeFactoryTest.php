<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactInfoType;

/**
 * Tests the DocTypeRef_ShipType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ShipTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_ShipTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_ShipType = DocTypeRef_ShipTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_ShipType::class, $docTypeRef_ShipType);

    $this->assertInstanceOf(DocTypeRef_ContactInfoType::class, $docTypeRef_ShipType->getShipper());
    $this->assertInstanceOf(DocTypeRef_ContactInfoType::class, $docTypeRef_ShipType->getRecipient());
  }

}
