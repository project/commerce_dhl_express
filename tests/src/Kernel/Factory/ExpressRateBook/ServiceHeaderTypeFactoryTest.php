<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\ServiceHeaderType;

/**
 * Tests the ServiceHeaderType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\ServiceHeaderTypeFactory
 * @group commerce_dhl_express
 */
final class ServiceHeaderTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The time.
   *
   * @var \Drupal\Core\Datetime\Time
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->dateFormatter = $this->container->get('date.formatter');
    $this->time = $this->container->get('datetime.time');
  }

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $serviceHeaderType = ServiceHeaderTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(ServiceHeaderType::class, $serviceHeaderType);

    /** @var \Drupal\commerce_dhl_express\Plugin\Commerce\ShippingMethod\DhlExpressInterface $shipping_method_plugin */
    $shipping_method_plugin = $this->shipment->getShippingMethod()->getPlugIn();

    $this->assertEquals(
      $this->dateFormatter->format(
        $this->time->getCurrentTime(),
        'custom',
        'Y-m-d\TH:i:sP'
      ),
      $serviceHeaderType->getMessageTime()
    );
    $this->assertEquals($shipping_method_plugin::WEBSTORE_PLATFORM, $serviceHeaderType->getWebstorePlatform());
    $this->assertEquals($shipping_method_plugin::SHIPPING_SYSTEM_PLATFORM, $serviceHeaderType->getShippingSystemPlatform());
    $this->assertEquals($shipping_method_plugin::PLUG_IN, $serviceHeaderType->getPlugIn());
  }

}
