<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipType2;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType2;

/**
 * Tests the DocTypeRef_ShipType2 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ShipType2Factory
 * @group commerce_dhl_express
 */
final class DocTypeRef_ShipType2FactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_ShipType2 = DocTypeRef_ShipType2Factory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_ShipType2::class, $docTypeRef_ShipType2);

    $this->assertInstanceOf(DocTypeRef_AddressType2::class, $docTypeRef_ShipType2->getShipper());
    $this->assertInstanceOf(DocTypeRef_AddressType2::class, $docTypeRef_ShipType2->getRecipient());
  }

}
