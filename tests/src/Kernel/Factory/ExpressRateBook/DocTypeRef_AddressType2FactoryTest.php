<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_AddressType2Factory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType2;

/**
 * Tests the DocTypeRef_AddressType2 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_AddressType2Factory
 * @group commerce_dhl_express
 */
final class DocTypeRef_AddressType2FactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createRecipientAddressFromShipment
   */
  public function testCreateRecipientAddressFromShipment(): void {
    $docTypeRef_AddressType2 = DocTypeRef_AddressType2Factory::createRecipientAddressFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_AddressType2::class, $docTypeRef_AddressType2);

    $address = $this->shippingProfile->get('address')->first()->getValue();
    $this->assertEquals($address['locality'], $docTypeRef_AddressType2->getCity());
    $this->assertEquals($address['postal_code'], $docTypeRef_AddressType2->getPostalCode());
    $this->assertEquals($address['country_code'], $docTypeRef_AddressType2->getCountryCode());
    $this->assertInstanceOf(DocTypeRef_ContactType::class, $docTypeRef_AddressType2->getContact());
  }

  /**
   * @covers ::createShipperAddressFromShipment
   */
  public function testCreateShipperAddressFromShipment(): void {
    $docTypeRef_AddressType2 = DocTypeRef_AddressType2Factory::createShipperAddressFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_AddressType2::class, $docTypeRef_AddressType2);

    $shipping_method_config = $this->shippingMethod->getPlugIn()->getConfiguration();
    $address = $shipping_method_config['api_information']['requested_shipment']['ship']['shipper']['address'];
    $this->assertEquals(
      $address['address_line1'],
      $docTypeRef_AddressType2->getStreetLines()
    );
    $this->assertEquals(
      $address['address_line2'],
      $docTypeRef_AddressType2->getStreetLines2()
    );
    $this->assertEquals($address['postal_code'], $docTypeRef_AddressType2->getPostalCode());
    $this->assertEquals($address['country_code'], $docTypeRef_AddressType2->getCountryCode());
    $this->assertInstanceOf(DocTypeRef_ContactType::class, $docTypeRef_AddressType2->getContact());
  }

}
