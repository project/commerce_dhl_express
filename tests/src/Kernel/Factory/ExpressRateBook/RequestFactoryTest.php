<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\Request;
use Maetva\DhlExpress\ExpressRateBook\StructType\ServiceHeaderType;

/**
 * Tests the Request Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\RequestFactory
 * @group commerce_dhl_express
 */
final class RequestFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $request = RequestFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(Request::class, $request);

    $this->assertInstanceOf(ServiceHeaderType::class, $request->getServiceHeader());
  }

}
