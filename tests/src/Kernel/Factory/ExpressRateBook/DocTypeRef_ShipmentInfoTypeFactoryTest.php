<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\EnumType\DropOffType;
use Maetva\DhlExpress\ExpressRateBook\StructType\LabelOptions;
use Maetva\DhlExpress\ExpressRateBook\EnumType\UnitOfMeasurement;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipmentInfoType;

/**
 * Tests the DocTypeRef_ShipmentInfoType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ShipmentInfoTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_ShipmentInfoTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_ShipmentInfoType = DocTypeRef_ShipmentInfoTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_ShipmentInfoType::class, $docTypeRef_ShipmentInfoType);

    $shipping_method_config = $this->shipment->getShippingMethod()->getPlugIn()->getConfiguration();

    $this->assertEquals(DropOffType::VALUE_REGULAR_PICKUP, $docTypeRef_ShipmentInfoType->getDropOffType());
    $this->assertEquals($this->shipment->getShippingService(), $docTypeRef_ShipmentInfoType->getServiceType());
    $this->assertEquals(
      $shipping_method_config['api_information']['credentials']['account'],
      $docTypeRef_ShipmentInfoType->getAccount()
    );
    $this->assertEquals($this->shipment->getAmount()->getCurrencyCode(), $docTypeRef_ShipmentInfoType->getCurrency());
    $this->assertEquals(UnitOfMeasurement::VALUE_SI, $docTypeRef_ShipmentInfoType->getUnitOfMeasurement());
    $this->assertInstanceOf(LabelOptions::class, $docTypeRef_ShipmentInfoType->getLabelOptions());
    $this->assertEquals('ECOM26_84_A4_001', $docTypeRef_ShipmentInfoType->getLabelTemplate());
  }

}
