<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\RequestType;
use Maetva\DhlExpress\ExpressRateBook\StructType\ServiceHeaderType;

/**
 * Tests the RequestType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\RequestTypeFactory
 * @group commerce_dhl_express
 */
final class RequestTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $requestType = RequestTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(RequestType::class, $requestType);

    $this->assertInstanceOf(ServiceHeaderType::class, $requestType->getServiceHeader());
  }

}
