<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\EnumType\Content;
use Maetva\DhlExpress\ExpressRateBook\EnumType\DropOffType2;
use Maetva\DhlExpress\ExpressRateBook\EnumType\PaymentInfo2;
use Maetva\DhlExpress\ExpressRateBook\EnumType\NextBusinessDay2;
use Maetva\DhlExpress\ExpressRateBook\EnumType\UnitOfMeasurement2;
use Maetva\DhlExpress\ExpressRateBook\EnumType\CustomerAgreementInd2;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipType2;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_PackagesType2;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedShipmentType2;

/**
 * Tests the DocTypeRef_RequestedShipmentType2 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RequestedShipmentType2Factory
 * @group commerce_dhl_express
 */
final class DocTypeRef_RequestedShipmentType2FactoryTest extends DhlExpressKernelTestBase {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The time.
   *
   * @var \Drupal\Core\Datetime\Time
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->dateFormatter = $this->container->get('date.formatter');
    $this->time = $this->container->get('datetime.time');
  }

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_RequestedShipmentType2 = DocTypeRef_RequestedShipmentType2Factory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_RequestedShipmentType2::class, $docTypeRef_RequestedShipmentType2);

    $shipping_method_plugin = $this->shipment->getShippingMethod()->getPlugIn();
    $shipping_method_config = $shipping_method_plugin->getConfiguration();

    $this->assertEquals(NextBusinessDay2::VALUE_Y, $docTypeRef_RequestedShipmentType2->getNextBusinessDay());
    $this->assertEquals($this->shipment->getShippingService(), $docTypeRef_RequestedShipmentType2->getServiceType());
    $this->assertEquals(CustomerAgreementInd2::VALUE_N, $docTypeRef_RequestedShipmentType2->getCustomerAgreementInd());
    $this->assertEquals(DropOffType2::VALUE_REGULAR_PICKUP, $docTypeRef_RequestedShipmentType2->getDropOffType());
    $this->assertEquals(
      $this->dateFormatter->format(
        $this->time->getCurrentTime() + 60,
        'custom',
        'Y-m-d\TH:i:s\G\M\TP'
      ),
      $docTypeRef_RequestedShipmentType2->getShipTimestamp()
    );
    $this->assertEquals(UnitOfMeasurement2::VALUE_SI, $docTypeRef_RequestedShipmentType2->getUnitOfMeasurement());
    $this->assertEquals(PaymentInfo2::VALUE_DAP, $docTypeRef_RequestedShipmentType2->getPaymentInfo());
    $this->assertEquals(
      $shipping_method_config['api_information']['credentials']['account'],
      $docTypeRef_RequestedShipmentType2->getAccount()
    );
    $this->assertInstanceOf(DocTypeRef_ShipType2::class, $docTypeRef_RequestedShipmentType2->getShip());
    $this->assertInstanceOf(DocTypeRef_PackagesType2::class, $docTypeRef_RequestedShipmentType2->getPackages());
    $this->assertEquals(
      Content::VALUE_DOCUMENTS,
      $docTypeRef_RequestedShipmentType2->getContent()
    ); // @todo Test with EU countries and Content::VALUE_NON_DOCUMENTS.
    $this->assertEquals(
      $this->shipment->getTotalDeclaredValue()->getNumber(),
      $docTypeRef_RequestedShipmentType2->getDeclaredValue()
    );
    $this->assertEquals(
      $this->shipment->getTotalDeclaredValue()->getCurrencyCode(),
      $docTypeRef_RequestedShipmentType2->getDeclaredValueCurrencyCode()
    );
  }

}
