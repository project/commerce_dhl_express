<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\LabelOptions;
use Maetva\DhlExpress\ExpressRateBook\EnumType\RequestLabelsToFitA4;

/**
 * Tests the LabelOptions Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\LabelOptionsFactory
 * @group commerce_dhl_express
 */
final class LabelOptionsFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $labelOptions = LabelOptionsFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(LabelOptions::class, $labelOptions);

    $this->assertEquals(RequestLabelsToFitA4::VALUE_Y, $labelOptions->getRequestLabelsToFitA4());
  }

}
