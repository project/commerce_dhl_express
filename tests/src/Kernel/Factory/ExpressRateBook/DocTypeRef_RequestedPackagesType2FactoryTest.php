<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_WeightType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_DimensionsType2;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedPackagesType2;

/**
 * Tests the DocTypeRef_RequestedPackagesType2 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RequestedPackagesType2Factory
 * @group commerce_dhl_express
 */
final class DocTypeRef_RequestedPackagesType2FactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_RequestedPackagesType2 = DocTypeRef_RequestedPackagesType2Factory::createFromShipment($this->shipment);

    $this->assertInstanceOf(DocTypeRef_RequestedPackagesType2::class, $docTypeRef_RequestedPackagesType2);
    $this->assertEquals(1, $docTypeRef_RequestedPackagesType2->getNumber());
    $this->assertInstanceOf(DocTypeRef_WeightType::class, $docTypeRef_RequestedPackagesType2->getWeight());
    $this->assertInstanceOf(DocTypeRef_DimensionsType2::class, $docTypeRef_RequestedPackagesType2->getDimensions());
  }

}
