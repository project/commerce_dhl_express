<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_PackagesTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_PackagesType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedPackagesType;

/**
 * Tests the DocTypeRef_PackagesType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_PackagesTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_PackagesTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_PackagesType = DocTypeRef_PackagesTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_PackagesType::class, $docTypeRef_PackagesType);

    $this->assertInstanceOf(DocTypeRef_RequestedPackagesType::class, $docTypeRef_PackagesType->getRequestedPackages());
  }

}
