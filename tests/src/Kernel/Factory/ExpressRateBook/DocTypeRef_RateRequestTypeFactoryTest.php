<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\RequestType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateRequestType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedShipmentType2;

/**
 * Tests the DocTypeRef_RateRequestType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RateRequestTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_RateRequestTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_RateRequestType = DocTypeRef_RateRequestTypeFactory::createFromShipment($this->shipment);

    $this->assertInstanceOf(DocTypeRef_RateRequestType::class, $docTypeRef_RateRequestType);
    $this->assertInstanceOf(RequestType::class, $docTypeRef_RateRequestType->getRequest());
    $this->assertInstanceOf(DocTypeRef_RequestedShipmentType2::class, $docTypeRef_RateRequestType->getRequestedShipment());
  }

}
