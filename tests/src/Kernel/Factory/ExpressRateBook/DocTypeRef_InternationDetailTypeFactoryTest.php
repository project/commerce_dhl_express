<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\EnumType\Content;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_InternationDetailTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_CommoditiesType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_InternationDetailType;

/**
 * Tests the DocTypeRef_InternationDetailType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_InternationDetailTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_InternationDetailTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_InternationDetailType = DocTypeRef_InternationDetailTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_InternationDetailType::class, $docTypeRef_InternationDetailType);

    $this->assertInstanceOf(DocTypeRef_CommoditiesType::class, $docTypeRef_InternationDetailType->getCommodities());
    $this->assertEquals(
      Content::VALUE_DOCUMENTS,
      $docTypeRef_InternationDetailType->getContent()
    ); // @todo Test with EU countries and Content::VALUE_NON_DOCUMENTS.

  }

}
