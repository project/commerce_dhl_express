<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_dhl_express\Kernel\Factory\ExpressRateBook;

use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_CommoditiesTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_CommoditiesType;

/**
 * Tests the DocTypeRef_CommoditiesType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_CommoditiesTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_CommoditiesTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_CommoditiesType = DocTypeRef_CommoditiesTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_CommoditiesType::class, $docTypeRef_CommoditiesType);

    $shipping_method_config = $this->shippingMethod->getPlugIn()->getConfiguration();
    $commodities = $shipping_method_config['api_information']['requested_shipment']['international_detail']['commodities'];
    $this->assertEquals($commodities['description'], $docTypeRef_CommoditiesType->getDescription());
    $this->assertEquals(
      $this->shipment->getTotalDeclaredValue()->getNumber(),
      $docTypeRef_CommoditiesType->getCustomsValue()
    );
  }

}
