<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\physical\Calculator;
use Drupal\physical\WeightUnit;
use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_DimensionsType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedPackagesType;

/**
 * Tests the DocTypeRef_RequestedPackagesType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RequestedPackagesTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_RequestedPackagesTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_RequestedPackagesType = DocTypeRef_RequestedPackagesTypeFactory::createFromShipment($this->shipment);

    $this->assertInstanceOf(DocTypeRef_RequestedPackagesType::class, $docTypeRef_RequestedPackagesType);
    $this->assertEquals(
      (float) Calculator::round($this->shipment->getWeight()->convert(WeightUnit::KILOGRAM)->getNumber(), 3),
      $docTypeRef_RequestedPackagesType->getWeight()
    );
    $this->assertInstanceOf(DocTypeRef_DimensionsType::class, $docTypeRef_RequestedPackagesType->getDimensions());
  }

}
