<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\physical\Calculator;
use Drupal\physical\WeightUnit;
use Drupal\Tests\commerce_dhl_express\Kernel\DhlExpressKernelTestBase;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_WeightType;

/**
 * Tests the DocTypeRef_WeightType Factory.
 *
 * @coversDefaultClass \Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_WeightTypeFactory
 * @group commerce_dhl_express
 */
final class DocTypeRef_WeightTypeFactoryTest extends DhlExpressKernelTestBase {

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $docTypeRef_WeightType = DocTypeRef_WeightTypeFactory::createFromShipment($this->shipment);
    $this->assertInstanceOf(DocTypeRef_WeightType::class, $docTypeRef_WeightType);

    $this->assertEquals(
      (float) Calculator::round($this->shipment->getWeight()->convert(WeightUnit::KILOGRAM)->getNumber(), 3),
      $docTypeRef_WeightType->getValue()
    );
  }

}
