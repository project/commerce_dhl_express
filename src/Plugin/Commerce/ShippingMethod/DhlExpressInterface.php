<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Plugin\Commerce\ShippingMethod;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateResponseType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipmentDetailType;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackShipmentRequestResponse;

/**
 * Provides the DHL Express shipping method interface.
 */
interface DhlExpressInterface extends SupportsTrackingInterface {

  /**
   * The module related logger channel.
   */
  const LOGGER_CHANNEL = 'commerce_dhl_express';

  /**
   * The shipment labels stroage directory.
   */
  const LABEL_DIR = 'private://dhl/express';

  /**
   * The "webstore platform" parameter value.
   */
  const WEBSTORE_PLATFORM = 'drupal';

  /**
   * The "shipping system platform" request parameter value.
   */
  const SHIPPING_SYSTEM_PLATFORM = 'commerce_shipping';

  /**
   * The "plug-in" request parameter value.
   */
  const PLUG_IN = 'commerce_dhl_express';

  /**
   * The EU country codes.
   */
  const EU_COUNTRY_CODES = [
    'Austria' => 'AT',
    'Belgium' => 'BE',
    'Bulgaria' => 'BG',
    'Croatia' => 'HR',
    'Cyprus' => 'CY',
    'Czech Republic' => 'CZ',
    'Denmark' => 'DK',
    'Estonia' => 'EE',
    'Finland' => 'FI',
    'France' => 'FR',
    'Germany' => 'DE',
    'Greece' => 'GR',
    'Hungary' => 'HU',
    'Ireland' => 'IE',
    'Italy' => 'IT',
    'Latvia' => 'LV',
    'Lithuania' => 'LT',
    'Luxembourg' => 'LU',
    'Malta' => 'MT',
    'Netherlands' => 'NL',
    'Poland' => 'PL',
    'Portugal' => 'PT',
    'Romania' => 'RO',
    'Slovakia' => 'SK',
    'Slovenia' => 'SI',
    'Spain' => 'ES',
    'Sweden' => 'SE',
  ];

  /**
   * Refreshes the rating options on rating mode selection.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The buildable array of the rating options form part.
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state): array;

  /**
   * Creates a DHL shipment request from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipmentDetailType|null
   *   The shipment request result.
   */
  public function createRemote(ShipmentInterface $shipment): ?DocTypeRef_ShipmentDetailType;

  /**
   * Gets a shipment Label filename.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param string $awb_number
   *   The AWB number.
   *
   * @return string
   *   The filename.
   */
  public static function getLabelFilename(ShipmentInterface $shipment, string $awb_number): string;

  /**
   * Performs a rate request from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Drupal\Console\Bootstrap\Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateResponseType|null
   *   The rate request result.
   */
  public function rate(ShipmentInterface $shipment): ?DocTypeRef_RateResponseType;

  /**
   * Tracks a shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackShipmentRequestResponse|null
   *   The tracking request result.
   */
  public function track(ShipmentInterface $shipment): ?TrackShipmentRequestResponse;

}
