<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Plugin\Commerce\ShippingMethod;

use Drupal\Core\Url;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_dhl_express\ExpressRateBook;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_dhl_express\GlDHLExpressTrack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_dhl_express\Event\PreRateRequestEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_dhl_express\Event\PreShipmentRequestEvent;
use Drupal\commerce_dhl_express\Event\CommerceDhlExpressEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack\TrackShipmentRequestFactory;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RateRequestTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_LabelImageType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateResponseType;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipmentDetailType;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackShipmentRequestResponse;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ProcessShipmentRequestTypeFactory;

/**
 * Provides the DHL Express shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "dhl_express",
 *   label = @Translation("DHL Express"),
 *   workflow = "commerce_dhl_express",
 *   services = {
 *     "N" = @Translation("Domestic Express"),
 *     "P" = @Translation("Express Wordwilde"),
 *     "U" = @Translation("Express Wordwilde (EU)"),
 *   }
 * )
 */
class DhlExpress extends ShippingMethodBase implements DhlExpressInterface {

  /**
   * Constructs a new ShippingMethodBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, ExpressRateBook $express_rate_book, GlDHLExpressTrack $gl_dhl_express_track, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, FileRepositoryInterface $file_repository, LoggerChannelFactoryInterface $logger_channel_factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->expressRateBook = $express_rate_book;
    $this->glDHLExpressTrack = $gl_dhl_express_track;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
    $this->loggerChannel = $logger_channel_factory->get(self::LOGGER_CHANNEL);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_dhl_express.express_rate_book'),
      $container->get('commerce_dhl_express.gl_dhl_express_track'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('logger.factory'),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mode' => 'test',
      'api_information' => [
          'credentials' => [
          'account' => '',
          'username' => '',
          'password' => '',
        ],
        'requested_shipment' => [
          'ship' => [
            'shipper' => [
              'contact' => [
                'person_name' => '',
                'company_name' => '',
                'phone_number' => '',
                'email_address' => '',
                'address' => [],
              ],
              'address' => [],
            ],
          ],
          'international_detail' => [
            'commodities' => [
              'description' => '',
            ]
          ]
        ],
      ],
      'options' => [
        'tracking_url' => 'https://www.dhl.com/global-en/home/tracking.html?tracking-id=[tracking_code]',
      ],
      'rating' => [
        'mode' => 'flat',
        'options' => [
          'flat' => [
            'rate_amount' => NULL,
          ],
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Select all services by default.
    if (empty($this->configuration['services'])) {
      $service_ids = array_keys($this->services);
      $this->configuration['services'] = array_combine($service_ids, $service_ids);
    }
    $is_new = $form_state->getBuildInfo()['callback_object']->getEntity()->isNew();

    $form['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Choose whether to use the test or live mode.'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $this->configuration['mode'],
    ];
    $form['api_information'] = [
      '#type' => 'details',
      '#title' => $this->t('API information'),
      '#open' => $is_new,
    ];
    $api_information = &$form['api_information'];
    $api_information['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Credentials'),
    ];
    $credentials = &$api_information['credentials'];
    $credentials_config = $this->configuration['api_information']['credentials'];
    $credentials['account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account number'),
      '#maxlength' => 12,
      '#default_value' => $credentials_config['account'],
      '#required' => TRUE,
    ];
    $credentials['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $credentials_config['username'],
      '#required' => TRUE,
    ];
    $credentials['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $credentials_config['password'],
      '#required' => TRUE,
    ];

    $api_information['requested_shipment'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Requested shipment'),
    ];
    $requested_shipment = &$api_information['requested_shipment'];
    $requested_shipment_config = $this->configuration['api_information']['requested_shipment'];
    $requested_shipment['ship'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Ship'),
    ];
    $requested_shipment['ship']['shipper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Shipper'),
    ];
    // More constraints added to address in the .module file.
    $requested_shipment['ship']['shipper']['address'] = [
      '#type' => 'address',
      '#title' => $this->t('Address'),
      '#default_value' => $requested_shipment_config['ship']['shipper']['address'],
      '#required' => TRUE,
      '#field_overrides' => [
        AddressField::ADMINISTRATIVE_AREA => FieldOverride::HIDDEN,
        AddressField::DEPENDENT_LOCALITY => FieldOverride::HIDDEN,
        AddressField::SORTING_CODE => FieldOverride::HIDDEN,
        AddressField::ADDITIONAL_NAME => FieldOverride::HIDDEN,
        AddressField::ORGANIZATION => FieldOverride::HIDDEN,
        AddressField::GIVEN_NAME => FieldOverride::HIDDEN,
        AddressField::FAMILY_NAME => FieldOverride::HIDDEN,
      ],
    ];
    $requested_shipment['ship']['shipper']['contact'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Contact'),
    ];
    $requested_shipment['ship']['shipper']['contact']['person_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $requested_shipment_config['ship']['shipper']['contact']['person_name'],
      '#required' => TRUE,
    ];
    $requested_shipment['ship']['shipper']['contact']['company_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Organization'),
      '#maxlength' => 100,
      '#default_value' => $requested_shipment_config['ship']['shipper']['contact']['company_name'],
      '#required' => TRUE,
    ];
    $requested_shipment['ship']['shipper']['contact']['email_address'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#maxlength' => 70,
      '#default_value' => $requested_shipment_config['ship']['shipper']['contact']['email_address'],
      '#required' => TRUE,
    ];
    $requested_shipment['ship']['shipper']['contact']['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#maxlength' => 70,
      '#default_value' => $requested_shipment_config['ship']['shipper']['contact']['phone_number'],
      '#required' => TRUE,
    ];
    $requested_shipment['international_detail'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('International detail'),
    ];
    $requested_shipment['international_detail']['commodities'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Commodities'),
    ];
    $requested_shipment['international_detail']['commodities']['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#maxlength' => 70,
      '#default_value' => $requested_shipment_config['international_detail']['commodities']['description'],
      '#required' => TRUE,
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Options'),
      '#open' => $is_new,
    ];
    $options = &$form['options'];
    $options_config = $this->configuration['options'];
    $options['tracking_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tracking URL base'),
      '#description' => $this->t(
        'The base URL for assembling a tracking URL. If the [tracking_code]
        token is omitted, the code will be appended to the end of the URL
        (e.g. "https://www.dhl.com/global-en/home/tracking.html?tracking-id=123456789")'
      ),
      '#default_value' => $options_config['tracking_url'],
    ];

    $form['rating'] = [
      '#type' => 'details',
      '#title' => $this->t('Rating'),
      '#open' => TRUE,
    ];
    $rating = &$form['rating'];
    $rating_config = $this->configuration['rating'];
    $rating['mode'] = [
      '#type' => 'radios',
      '#default_value' => $rating_config['mode'],
      '#options' => [
        'flat' => $this->t('Flat rate'),
        // 'dynamic' => $this->t('Dynamic rate'), // @todo Implements dynamic rates.
      ],
      '#title' => $this->t('Rating'),
      '#ajax' => [
        'wrapper' => 'rating_options',
        'callback' => [$this, 'ajaxRefresh'],
      ],
    ];
    $rating['options'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'rating_options',
      ],
    ];

    $selected_rating_mode = $form_state->getValue(
      array_merge($form['#parents'], ['rating', 'mode'])
    );
    $rating_mode = $selected_rating_mode ?? $rating['mode']['#default_value'];

    switch ($rating_mode) {
      case 'flat':
        $amount = $rating_config['options']['flat']['rate_amount'];
        // A bug in the plugin_select form element causes $amount to be incomplete.
        if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
          $amount = NULL;
        }

        $rating['options']['flat']['rate_amount'] = [
          '#type' => 'commerce_price',
          '#title' => $this->t('Rate amount'),
          '#default_value' => $amount,
          '#required' => TRUE,
        ];
        break;
    }

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state): array {
    return $form['plugin']['widget'][0]['target_plugin_configuration']['form']['rating']['options'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    if (mb_strlen($values['api_information']['requested_shipment']['international_detail']['commodities']['description']) > 70) {
      $form_state->setErrorByName(
        implode('][', $form['#parents']) . '][api_information][requested_shipment][international_detail][commodities][description',
        $this->t('The description must not exceed 70 characters.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['mode'] = $values['mode'];
      $this->configuration['api_information']['credentials']['account'] = $values['api_information']['credentials']['account'];
      $this->configuration['api_information']['credentials']['username'] = $values['api_information']['credentials']['username'];
      $this->configuration['api_information']['credentials']['password'] = $values['api_information']['credentials']['password'];
      $this->configuration['api_information']['requested_shipment']['ship']['shipper']['contact']['person_name'] = $values['api_information']['requested_shipment']['ship']['shipper']['contact']['person_name'];
      $this->configuration['api_information']['requested_shipment']['ship']['shipper']['contact']['company_name'] = $values['api_information']['requested_shipment']['ship']['shipper']['contact']['company_name'];
      $this->configuration['api_information']['requested_shipment']['ship']['shipper']['contact']['phone_number'] = $values['api_information']['requested_shipment']['ship']['shipper']['contact']['phone_number'];
      $this->configuration['api_information']['requested_shipment']['ship']['shipper']['contact']['email_address'] = $values['api_information']['requested_shipment']['ship']['shipper']['contact']['email_address'];
      $this->configuration['api_information']['requested_shipment']['ship']['shipper']['address'] = $values['api_information']['requested_shipment']['ship']['shipper']['address'];
      $this->configuration['api_information']['requested_shipment']['international_detail']['commodities']['description'] = $values['api_information']['requested_shipment']['international_detail']['commodities']['description'];
      $this->configuration['options']['tracking_url'] = $values['options']['tracking_url'];
      $this->configuration['rating']['mode'] = $values['rating']['mode'];
      if (isset($values['rating']['options']['flat']['rate_amount'])) {
        $this->configuration['rating']['options']['flat']['rate_amount'] = $values['rating']['options']['flat']['rate_amount'];
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getTrackingUrl(ShipmentInterface $shipment): ?Url {
    $code = $shipment->getTrackingCode();

    if (!empty($code)) {
      // If the tracking code token exists, replace it with the code.
      if (strstr($this->configuration['options']['tracking_url'], '[tracking_code]')) {
        $url = str_replace('[tracking_code]', $code, $this->configuration['options']['tracking_url']);
        return Url::fromUri($url);
      }

      // Otherwise, append the tracking code to the end of the URL.
      $url = $this->configuration['options']['tracking_url'] . $code;
      return Url::fromUri($url);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $rates = [];

    switch ($this->configuration['rating']['mode']) {
      case 'flat':
        foreach ($this->configuration['services'] as $service_id) {
          $rates[] = new ShippingRate([
            'shipping_method_id' => $this->parentEntity->id(),
            'service' => $this->services[$service_id],
            'amount' => Price::fromArray($this->configuration['rating']['options']['flat']['rate_amount']),
            'description' => $this->services[$service_id]->getLabel(),
          ]);
        }
        break;

      // @todo Implements dynamic rates.
      /* case 'dynamic':
        break; */

      default:
        break;
    }

    return $rates;
  }

  /**
   * {@inheritDoc}
   */
  public function createRemote(ShipmentInterface $shipment): ?DocTypeRef_ShipmentDetailType {
    $web_service = $this->expressRateBook->get(
      $this->configuration['api_information']['credentials']['username'],
      $this->configuration['api_information']['credentials']['password'],
      $this->configuration['mode']
    );
    try {
      $parameters = DocTypeRef_ProcessShipmentRequestTypeFactory::createFromShipment($shipment);
    }
    catch (\Exception $e) {
      return NULL;
    }

    $event = new PreShipmentRequestEvent($shipment, $parameters);
    $this->eventDispatcher->dispatch(
      $event,
      CommerceDhlExpressEvents::PRE_SHIPMENT_REQUEST
    );
    $parameters = $event->getParameters();

    if ($web_service->createShipmentRequest($parameters) === FALSE) {
      $last_error = $web_service->getLastError();
      $this->loggerChannel->error($last_error);
      return NULL;
    }

    $result = $web_service->getResult();

    if ($result->getPackagesResult() === NULL) {
      $notifications = $result->getNotification();
      foreach ($notifications as $notification) {
        $this->loggerChannel->error(
          $notification->getCode() . ': ' . $notification->getMessage()
        );
      }
      return NULL;
    }

    $this->processShipmentRequestResult($shipment, $web_service->getResult());

    return $result;
  }

  /**
   * Processes a successful shipment request.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipmentDetailType $result
   *   The request result.
   */
  protected function processShipmentRequestResult(ShipmentInterface $shipment, DocTypeRef_ShipmentDetailType $result): void {
    $tracking_number = $result
      ->getPackagesResult()
      ->getPackageResult()[0]
      ->getTrackingNumber();
    $awb_number = $result->getShipmentIdentificationNumber();
    $this->setShipmentTrackingCode($shipment, $awb_number);
    $this->storeShipmentLabel(
      $shipment,
      $awb_number,
      $result->getLabelImage()[0]
    );
    $this->transitonateToPacked($shipment);
    $this->logShipment(
      $shipment->getOrder(),
      $shipment->id(),
      $awb_number,
      $tracking_number
    );
  }

  /**
   * Sets the shipment tracking code.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param string $awb_number
   *   The AWB number.
   */
  protected function setShipmentTrackingCode(ShipmentInterface $shipment, string $awb_number): void {
    $shipment->setTrackingCode($awb_number);
    $shipment->save();
  }

  /**
   * Log the shipment request into the shipment related order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The shipment related order entity.
   * @param string $shipment_id
   *   The shipment shipment id.
   * @param string $awb_number
   *   The AWB number.
   * @param string $tracking_number
   *   The tracking number.
   */
  protected function logShipment(OrderInterface $order, string $shipment_id, string $awb_number, string $tracking_number): void {
    $this->entityTypeManager
      ->getStorage('commerce_log')
      ->generate($order, 'commerce_dhl_express_shipment_created', [
        'commerce_shipment_id' => $shipment_id,
        'dhl_tracking_number' => $tracking_number,
        'dhl_awb_number' => $awb_number,
      ])
      ->save();
  }

  /**
   * Stores the shipment PDF label.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param string $awb_number
   *   The tracking number.
   * @param \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_LabelImageType $label_image
   *   The label image object.
   */
  protected function storeShipmentLabel(ShipmentInterface $shipment, string $awb_number, DocTypeRef_LabelImageType $label_image): void {
    $pdf_label_dir = self::LABEL_DIR;
    $this->fileSystem->prepareDirectory(
      $pdf_label_dir,
      FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    );
    $this->fileRepository->writeData(
      $label_image->getGraphicImage(),
      $pdf_label_dir . '/' . self::getLabelFilename($shipment, $awb_number),
      FileSystemInterface::EXISTS_REPLACE
    );
  }

  /**
   * {@inheritDoc}
   */
  public static function getLabelFilename(ShipmentInterface $shipment, string $awb_number): string {
    return implode('-', [
      $shipment->getOrder()->id(),
      $shipment->id(),
      $awb_number,
    ]) . '.pdf';
  }

  /**
   * Set the shipment entity state according to the last DHL event code.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   */
  protected function transitonateToPacked(ShipmentInterface $shipment): void {
    if ($shipment->getState()->isTransitionAllowed('pack')) {
      $shipment->getState()->applyTransitionById('pack');
      $shipment->save();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function rate(ShipmentInterface $shipment): ?DocTypeRef_RateResponseType {
    $web_service = $this->expressRateBook->get(
      $this->configuration['api_information']['credentials']['username'],
      $this->configuration['api_information']['credentials']['password'],
      $this->configuration['mode']
    );
    $parameters = DocTypeRef_RateRequestTypeFactory::createFromShipment($shipment);
    $event = new PreRateRequestEvent($shipment, $parameters);
    $this->eventDispatcher->dispatch(
      $event,
      CommerceDhlExpressEvents::PRE_RATE_REQUEST
    );
    $parameters = $event->getParameters();

    if ($web_service->getRateRequest($parameters) === FALSE) {
      $last_error = $web_service->getLastError();
      $this->loggerChannel->error($last_error);
      return NULL;
    }

    $result = $web_service->getResult();

    if ($result->getProvider()->getService() === NULL) {
      $notifications = $result->getProvider()->getNotification();
      foreach ($notifications as $notification) {
        $this->loggerChannel->error(
          $notification->getCode() . ': ' . $notification->getMessage()
        );
      }
      return NULL;
    }

    return $result;
  }


  /**
   * {@inheritDoc}
   */
  public function track(ShipmentInterface $shipment): ?TrackShipmentRequestResponse {
    $web_service = $this->glDHLExpressTrack->get(
      $this->configuration['api_information']['credentials']['username'],
      $this->configuration['api_information']['credentials']['password'],
      $this->configuration['mode']
    );
    $parameters = TrackShipmentRequestFactory::createFromShipment($shipment);

    if ($web_service->trackShipmentRequest($parameters) !== FALSE) {
      $result = $web_service->getResult();
      $this->processTrackShipmentRequestResult($shipment, $result);
      return $result;
    }

    return NULL;
  }

  /**
   * Processes the tracking request response.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param \Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackShipmentRequestResponse $result
   *   The tracking request result.
   */
  protected function processTrackShipmentRequestResult(ShipmentInterface $shipment, TrackShipmentRequestResponse $result): void {
    $shipment_info = $result
      ->getTrackingResponse()
      ->getTrackingResponse()
      ->getAWBInfo()
      ->getArrayOfAWBInfoItem()[0]
      ->getShipmentInfo()
    ;

    if (!$shipment_info || !$shipment_event = $shipment_info->getShipmentEvent()) {
      return;
    }

    $events = $shipment_event->getArrayOfShipmentEventItem();
    $last_event = end($events);
    $service_event = $last_event->getServiceEvent();

    $this->setShipmentEntityState($shipment, $service_event->getEventCode());
  }

  /**
   * Set the shipment entity state according to the last DHL event code.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param string $event_code
   *   The DHL event code.
   */
  protected function setShipmentEntityState(ShipmentInterface $shipment, string $event_code): void {
    switch ($event_code) {
      case 'PU':
        if ($shipment->getState()->isTransitionAllowed('ship')) {
          $shipment->getState()->applyTransitionById('ship');
          $shipment->save();
        }
        break;

      case 'OK':
        if ($shipment->getState()->isTransitionAllowed('deliver')) {
          $shipment->getState()->applyTransitionById('deliver');
          $shipment->save();
        }
        break;
    }
  }

}
