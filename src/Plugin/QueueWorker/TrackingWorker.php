<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Provides a DHL shipments queue worker.
 *
 * @QueueWorker(
 *  id = "commerce_dhl_express_tracking",
 *  title = @Translation("DHL Express shipments tracking"),
 *  cron = {"time" = 30}
 * )
 */
class TrackingWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($shipments): void {
    foreach ($shipments as $shipment) {
      $shipment->getShippingMethod()->getPlugin()->track($shipment);
    }
  }

}
