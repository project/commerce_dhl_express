<?php

declare(strict_types=1);

namespace Drupal\commerce_dhl_express;

use WsdlToPhp\WsSecurity\WsSecurity;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;
use Maetva\DhlExpress\GlDHLExpressTrack\ClassMap;
use Maetva\DhlExpress\GlDHLExpressTrack\ServiceType\Service;

/**
 * The GlDHLExpressTrack consumer.
 */
class GlDHLExpressTrack extends Service implements WebServiceInterface
{
  /**
   * Gets the DHL service and set WSDL location + WS-Security headers.
   *
   * @param string $username
   * @param string $password
   * @param string $mode
   * @return self
   */
  public function get(string $username, string $password, string $mode = 'test'): self {
    $this->initSoapClient([
      AbstractSoapClientBase::WSDL_CLASSMAP => ClassMap::get(),
      AbstractSoapClientBase::WSDL_URL => dirname(__DIR__).DIRECTORY_SEPARATOR.'wsdl'.DIRECTORY_SEPARATOR.$mode.DIRECTORY_SEPARATOR.'glDHLExpressTrack.wsdl',
    ]);
    $this->getSoapClient()->__setSoapHeaders(
      WsSecurity::createWsSecuritySoapHeader($username, $password)
    );

    return $this;
  }
}
