<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Event;

/**
 * Defines events for the DHL Express module.
 */
final class CommerceDhlExpressEvents {

  /**
   * Event allowing alterations of the recipient contact on DHL shipment requests.
   *
   * @Event
   *
   * @see \Drupal\commerce_dhl_express\Event\PreShipmentRequestEvent
   */
  const PRE_SHIPMENT_REQUEST = 'commerce_dhl_express.pre_shipment_request';

  /**
   * Event allowing alterations of the recipient contact on DHL rating requests.
   *
   * @Event
   *
   * @see \Drupal\commerce_dhl_express\Event\PreRateRequestEvent
   */
  const PRE_RATE_REQUEST = 'commerce_dhl_express.pre_rate_request';

}
