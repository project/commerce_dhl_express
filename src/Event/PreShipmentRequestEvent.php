<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ProcessShipmentRequestType;

/**
 * Defines the recipient contact event for DHL shipment requests.
 *
 * @see \Drupal\commerce_dhl_express\Event\CommerceDhlExpressEvents
 * @see \Drupal\commerce_dhl_express\ExpressRateBook::getProcessShipmentRequestParameters()
 */
class PreShipmentRequestEvent extends Event {

  /**
   * The shipment entity.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * Thethe shipment request parameters.
   *
   * @var \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ProcessShipmentRequestType
   */
  protected $parameters;

  /**
   * Constructs a new ShipmentRequestRecipientContact instance.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ProcessShipmentRequestType $contact
   *   The recipient contact.
   */
  public function __construct(ShipmentInterface $shipment, DocTypeRef_ProcessShipmentRequestType $parameters) {
    $this->shipment = $shipment;
    $this->parameters = $parameters;
  }

  /**
   * Gets the shipment request parameters.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ProcessShipmentRequestType
   *   The shipment request parameters.
   */
  public function getParameters(): DocTypeRef_ProcessShipmentRequestType {
    return $this->parameters;
  }

  /**
   * Sets shipment request parameters.
   *
   * @param \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ProcessShipmentRequestType $parameters
   *   The shipment request parameters.
   *
   * @return $this
   */
  public function setParameters(DocTypeRef_ProcessShipmentRequestType $parameters): self {
    $this->parameters = $parameters;

    return $this;
  }

  /**
   * Gets the shipment entity.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment entity.
   */
  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

}
