<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateRequestType;

/**
 * Defines the recipient contact event for DHL rate requests.
 *
 * @see \Drupal\commerce_dhl_express\Event\CommerceDhlExpressEvents
 * @see \Drupal\commerce_dhl_express\ExpressRateBook::getRateRequestFromEntity()
 */
class PreRateRequestEvent extends Event {

  /**
   * The shipment entity.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * The the rate request parameters.
   *
   * @var \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateRequestType
   */
  protected $parameters;

  /**
   * Constructs a new ShipmentRequestRecipientContact instance.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateRequestType $contact
   *   The recipient contact.
   */
  public function __construct(ShipmentInterface $shipment, DocTypeRef_RateRequestType $parameters) {
    $this->shipment = $shipment;
    $this->parameters = $parameters;
  }

  /**
   * Gets the rate request parameters.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateRequestType
   *   The rate request parameters.
   */
  public function getParameters(): DocTypeRef_RateRequestType {
    return $this->parameters;
  }

  /**
   * Sets rate request parameters.
   *
   * @param \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateRequestType $parameters
   *   The rate request parameters.
   *
   * @return $this
   */
  public function setParameters(DocTypeRef_RateRequestType $parameters): self {
    $this->parameters = $parameters;

    return $this;
  }

  /**
   * Gets the shipment entity.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment entity.
   */
  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

}
