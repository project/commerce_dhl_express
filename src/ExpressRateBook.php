<?php

declare(strict_types=1);

namespace Drupal\commerce_dhl_express;

use WsdlToPhp\WsSecurity\WsSecurity;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;
use Maetva\DhlExpress\ExpressRateBook\ClassMap;
use Maetva\DhlExpress\ExpressRateBook\ServiceType\Service;

/**
 * The ExpressRateBook consumer.
 */
class ExpressRateBook extends Service implements WebServiceInterface
{
  /**
   * Gets the DHL service and set WSDL location + WS-Security headers.
   *
   * @param string $username
   * @param string $password
   * @param string $mode
   * @return self
   */
  public function get(string $username, string $password, string $mode = 'test'): self {
    $this->initSoapClient([
      AbstractSoapClientBase::WSDL_CLASSMAP => ClassMap::get(),
      AbstractSoapClientBase::WSDL_URL => dirname(__DIR__).DIRECTORY_SEPARATOR.'wsdl'.DIRECTORY_SEPARATOR.$mode.DIRECTORY_SEPARATOR.'expressRateBook.wsdl',
    ]);
    $this->getSoapClient()->__setSoapHeaders(
      WsSecurity::createWsSecuritySoapHeader($username, $password)
    );

    return $this;
  }

}
