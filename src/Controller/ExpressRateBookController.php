<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\commerce_dhl_express\ExpressRateBook;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ExpressRateBook related operations controller.
 */
class ExpressRateBookController extends ControllerBase {

  /**
   * Module logger channel ID.
   */
  const LOGGER_CHANNEL = 'commerce_dhl_express';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module related logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Create a new ShippingManagerController instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->loggerChannel = $logger_channel_factory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * Creates a DHL shipment request for a defined shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The shipment entity.
   *
   * @return array
   *   The shipment creation form buildable array.
   *
   * @todo Implements a rate request before the shipment request.
   * @see \Drupal\commerce_dhl_express\Form\ShipmentRequestForm
   */
  public function createShipment(ShipmentInterface $commerce_shipment): RedirectResponse {
    if ($commerce_shipment->getShippingMethod()->getPlugin()->createRemote($commerce_shipment)) {
      $this->messenger->addMessage($this->t('DHL shipping request succeeded'));
    }
    else {
      $this->messenger->addError($this->t('DHL shipping request failed'));
    }

    return $this->redirect('entity.commerce_shipment.canonical', [
      'commerce_order' => $commerce_shipment->getOrder()->id(),
      'commerce_shipment' => $commerce_shipment->id(),
    ]);
  }

  /**
   * Dwonloads the shipment label file for a defined shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The related commerce shipment entity.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The DHL shipment label.
   */
  public function downloadLabel(ShipmentInterface $commerce_shipment): BinaryFileResponse {
    $pdf_label_dir = $commerce_shipment->getShippingMethod()->getPlugIn()::LABEL_DIR;
    $pdf_label_filename = $commerce_shipment->getShippingMethod()->getPlugIn()->getLabelFilename(
      $commerce_shipment,
      $commerce_shipment->getTrackingCode()
    );
    $headers = [
      'Content-Type' => 'application/pdf',
      'Content-Disposition' => 'attachment; filename="' . $pdf_label_filename . '"',
    ];

    return new BinaryFileResponse($pdf_label_dir . '/' . $pdf_label_filename, 200, $headers, TRUE);
  }

}
