<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\commerce_dhl_express\GlDHLExpressTrack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GlDHLExpressTrack related operations controller.
 */
class GlDHLExpressTrackController extends ControllerBase {

  /**
   * Module logger channel ID.
   */
  const LOGGER_CHANNEL = 'commerce_dhl_express';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The DHL ExpressRateBook WS.
   *
   * @var \Drupal\commerce_dhl_express\GlDHLExpressTrack
   */
  protected $glDHLExpressTrack;

  /**
   * The module related logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Create a new ShippingManagerController instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->loggerChannel = $logger_channel_factory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * Tracks a DHL shipment for a defined shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The shipment entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the shipment canonical route.
   */
  public function trackShipment(ShipmentInterface $commerce_shipment): RedirectResponse {
    $commerce_shipment->getShippingMethod()->getPlugin()->track($commerce_shipment);

    return $this->redirect('entity.commerce_shipment.canonical', [
      'commerce_order' => $commerce_shipment->getOrder()->id(),
      'commerce_shipment' => $commerce_shipment->id(),
    ]);
  }

}
