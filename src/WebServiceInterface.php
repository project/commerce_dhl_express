<?php

declare(strict_types=1);

namespace Drupal\commerce_dhl_express;

/**
 * The web service interface.
 */
interface WebServiceInterface
{
  /**
   * Gets the DHL service with WSDL location + WS-Security headers set.
   *
   * @param string $username
   * @param string $password
   * @param string $mode
   *
   * @return self
   */
  public function get(string $username, string $password, string $mode = 'test'): self;

}
