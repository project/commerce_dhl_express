<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\commerce_dhl_express\ExpressRateBook;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages the DHL shipment request form.
 */
class ShipmentRequestForm extends FormBase {

  /**
   * The module related logger channel ID.
   */
  const LOGGER_CHANNEL = 'commerce_dhl_express';

  /**
   * The form ID.
   */
  const FORM_ID = 'commerce_dhl_express_form';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The DHL ExpressRateBook service.
   *
   * @var \Drupal\commerce_dhl_express\ExpressRateBook
   */
  protected $expressRateBook;

  /**
   * The module related logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Create a new ShippingManagerController instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger instance.
   * @param \Drupal\commerce_dhl_express\ExpressRateBook $express_rate_book
   *   The DHL rating/shipping service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, ExpressRateBook $express_rate_book, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->expressRateBook = $express_rate_book;
    $this->loggerChannel = $logger_channel_factory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('commerce_dhl_express.express_rate_book'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ShipmentInterface $commerce_shipment = NULL): array {
    $shipment = $commerce_shipment;
    $form['#shipment_entity'] = $shipment;

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * Builds the form actions.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The buildable array of form actions.
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = [
      '#type' => 'submit',
      '#rating' => TRUE,
      '#value' => $this->t('Get rates'),
    ];

    return $actions;
  }

  /**
   * Manages the form submission / DHL shipment request.
   *
   * @param array $form
   *   The form buildable array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $shipment = $form['#shipment_entity'];
    // Request a new DHL shipping.
    $response = $this->expressRateBook->createShipmentRequestFromEntity($shipment);
    /* $this->expressRateBook->getRateRequestFromEntity(
    $shipment,
    $shipment->getShippingMethod()->getPlugin()->getConfiguration(),
    $shipment->getShippingService()
    ); */
    $this->messenger->addMessage($this->t('DHL shipping created'));
    $form_state->setRedirect(
      'entity.commerce_shipment.canonical', [
        'commerce_order' => $shipment->getOrder()->id(),
        'commerce_shipment' => $shipment->id(),
      ]);
  }

}
