<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipType;

/**
 * DocTypeRef_ShipType Factory.
 */
final class DocTypeRef_ShipTypeFactory {

  /**
   * Constructs a new DocTypeRef_ShipType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipType
   *   The DocTypeRef_ShipTypeFactory instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_ShipType {
    return (new DocTypeRef_ShipType)
      ->setShipper(DocTypeRef_ContactInfoTypeFactory::createShipperFromShipment($shipment))
      ->setRecipient(DocTypeRef_ContactInfoTypeFactory::createRecipientFromShipment($shipment));
  }

}
