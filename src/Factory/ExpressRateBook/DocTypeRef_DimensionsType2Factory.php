<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\physical\LengthUnit;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_DimensionsType2;

/**
 * DocTypeRef_ContactInfoType Factory.
 */
final class DocTypeRef_DimensionsType2Factory {

  /**
   * Constructs a new DocTypeRef_ContactInfoType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_DimensionsType2
   *   The DocTypeRef_ContactInfoType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_DimensionsType2 {
    $package = $shipment->getPackageType();

    return (new DocTypeRef_DimensionsType2)
      ->setLength((float) $package->getLength()->convert(LengthUnit::METER)->getNumber())
      ->setWidth((float) $package->getWidth()->convert(LengthUnit::METER)->getNumber())
      ->setHeight((float) $package->getHeight()->convert(LengthUnit::METER)->getNumber());
  }

}
