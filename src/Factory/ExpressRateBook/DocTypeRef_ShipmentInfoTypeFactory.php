<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\LabelOptionsFactory;
use Maetva\DhlExpress\ExpressRateBook\EnumType\DropOffType;
use Maetva\DhlExpress\ExpressRateBook\EnumType\UnitOfMeasurement;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipmentInfoType;

/**
 * DocTypeRef_ShipmentInfoType Factory.
 */
final class DocTypeRef_ShipmentInfoTypeFactory {

  /**
   * Constructs a new DocTypeRef_ShipmentInfoType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipmentInfoType
   *   The DocTypeRef_ShipmentInfoType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_ShipmentInfoType {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugIn()->getConfiguration();

    return (new DocTypeRef_ShipmentInfoType)
      ->setDropOffType(DropOffType::VALUE_REGULAR_PICKUP)
      ->setServiceType($shipment->getShippingService())
      ->setAccount($shipping_method_config['api_information']['credentials']['account'])
      ->setCurrency($shipment->getAmount()->getCurrencyCode())
      ->setUnitOfMeasurement(UnitOfMeasurement::VALUE_SI)
      ->setLabelOptions(LabelOptionsFactory::createFromShipment($shipment))
      // @todo Implements label template custom settings
      ->setLabelTemplate('ECOM26_84_A4_001');
  }

}
