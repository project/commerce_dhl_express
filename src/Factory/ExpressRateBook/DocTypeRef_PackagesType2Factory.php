<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_PackagesType2;

/**
 * DocTypeRef_PackagesType2 Factory.
 */
final class DocTypeRef_PackagesType2Factory {

  /**
   * Constructs a new DocTypeRef_PackagesType2 instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_PackagesType2
   *   The DocTypeRef_PackagesType2 instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_PackagesType2 {
    return (new DocTypeRef_PackagesType2)
      ->setRequestedPackages(DocTypeRef_RequestedPackagesType2Factory::createFromShipment($shipment));
  }

}
