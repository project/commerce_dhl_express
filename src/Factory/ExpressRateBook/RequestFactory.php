<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\Request;

/**
 * Request factory.
 */
final class RequestFactory {

  /**
   * Constructs a new Request instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\Request
   *   The Request instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): Request {
    return (new Request)
      ->setServiceHeader(ServiceHeaderTypeFactory::createFromShipment($shipment));
  }

}
