<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\physical\Calculator;
use Drupal\physical\WeightUnit;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedPackagesType;

/**
 * DocTypeRef_RequestedPackagesType Factory.
 */
final class DocTypeRef_RequestedPackagesTypeFactory {

  /**
   * Constructs a new DocTypeRef_RequestedPackagesType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedPackagesType
   *   The DocTypeRef_RequestedPackagesType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_RequestedPackagesType {
    return (new DocTypeRef_RequestedPackagesType(1))
      ->setWeight((float) Calculator::round($shipment->getWeight()->convert(WeightUnit::KILOGRAM)->getNumber(), 3))
      ->setDimensions(DocTypeRef_DimensionsTypeFactory::createFromShipment($shipment));
  }

}
