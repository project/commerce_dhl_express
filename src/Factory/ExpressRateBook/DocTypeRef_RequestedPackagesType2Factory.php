<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_WeightTypeFactory;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_DimensionsType2Factory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedPackagesType2;

/**
 * DocTypeRef_RequestedPackagesType2 Factory.
 */
final class DocTypeRef_RequestedPackagesType2Factory {

  /**
   * Constructs a new DocTypeRef_RequestedPackagesType2 instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedPackagesType2
   *   The DocTypeRef_RequestedPackagesType2 instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_RequestedPackagesType2 {
    return (new DocTypeRef_RequestedPackagesType2)
      ->setNumber(1)
      ->setWeight(DocTypeRef_WeightTypeFactory::createFromShipment($shipment))
      ->setDimensions(DocTypeRef_DimensionsType2Factory::createFromShipment($shipment));
  }

}
