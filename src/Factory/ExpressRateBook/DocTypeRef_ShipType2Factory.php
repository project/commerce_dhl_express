<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_AddressType2Factory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipType2;

/**
 * DocTypeRef_ShipType2 Factory.
 */
final class DocTypeRef_ShipType2Factory {

  /**
   * Constructs a new DocTypeRef_ShipType2 instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ShipType2
   *   The DocTypeRef_ShipType2 instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_ShipType2 {
    return (new DocTypeRef_ShipType2)
      ->setShipper(DocTypeRef_AddressType2Factory::createShipperAddressFromShipment($shipment))
      ->setRecipient(DocTypeRef_AddressType2Factory::createRecipientAddressFromShipment($shipment));
  }

}
