<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ContactTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType2;

/**
 * DocTypeRef_AddressType2 Factory.
 */
final class DocTypeRef_AddressType2Factory {

  /**
   * Constructs a new DocTypeRef_AddressType2 instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType2
   *   The DocTypeRef_AddressType2 instance.
   */
  public static function createRecipientAddressFromShipment(ShipmentInterface $shipment): DocTypeRef_AddressType2 {
    $recipient_address = $shipment->getShippingProfile()->get('address')->first()->getValue();
    $recipient_country_code = $recipient_address['country_code'];

    return (new DocTypeRef_AddressType2)
      ->setCity($recipient_address['locality'])
      ->setPostalCode($recipient_address['postal_code'])
      ->setCountryCode($recipient_country_code)
      ->setContact(DocTypeRef_ContactTypeFactory::createRecipientContactFromShipment($shipment));
  }

  /**
   * Constructs a new DocTypeRef_AddressType2 instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType2
   *   The DocTypeRef_AddressType2 instance.
   */
  public static function createShipperAddressFromShipment(ShipmentInterface $shipment): DocTypeRef_AddressType2 {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugIn()->getConfiguration();
    $shipper_address = $shipping_method_config['api_information']['requested_shipment']['ship']['shipper']['address'];
    $docTypeRef_AddressType2 = (new DocTypeRef_AddressType2)
      ->setStreetLines($shipper_address['address_line1'])
      ->setCity($shipper_address['locality'])
      ->setPostalCode($shipper_address['postal_code'])
      ->setCountryCode($shipper_address['country_code'])
      ->setContact(DocTypeRef_ContactTypeFactory::createShipperContactFromShipment($shipment));

    if ($shipper_address['address_line2']) {
      $docTypeRef_AddressType2->setStreetLines2($shipper_address['address_line2']);
    }

    return $docTypeRef_AddressType2;
  }

}
