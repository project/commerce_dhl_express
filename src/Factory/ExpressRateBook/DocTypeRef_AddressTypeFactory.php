<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType;

/**
 * DocTypeRef_AddressType Factory.
 */
final class DocTypeRef_AddressTypeFactory {

  /**
   * Constructs a new DocTypeRef_AddressType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType
   *   The DocTypeRef_AddressType instance.
   */
  public static function createShipperAddressFromShipment(ShipmentInterface $shipment): DocTypeRef_AddressType {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugIn()->getConfiguration();
    $shipper_address = $shipping_method_config['api_information']['requested_shipment']['ship']['shipper']['address'];
    $country_list = \Drupal::getContainer()->get('country_manager')->getList();

    $docTypeRef_AddressType = (new DocTypeRef_AddressType)
      ->setStreetLines($shipper_address['address_line1'])
      ->setCity($shipper_address['locality'])
      ->setPostalCode($shipper_address['postal_code'])
      ->setCountryCode($shipper_address['country_code'])
      ->setCountryName($country_list[$shipper_address['country_code']]->render());

    if ($shipper_address['address_line2']) {
      $docTypeRef_AddressType->setStreetLines2($shipper_address['address_line2']);
    }

    return $docTypeRef_AddressType;
  }

  /**
   * Constructs a new DocTypeRef_AddressType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_AddressType
   *   The DocTypeRef_AddressType instance.
   */
  public static function createRecipientAddressFromShipment(ShipmentInterface $shipment): DocTypeRef_AddressType {
    $recipient_address = $shipment->getShippingProfile()->get('address')->first()->getValue();
    $country_list = \Drupal::getContainer()->get('country_manager')->getList();

    $docTypeRef_AddressType = (new DocTypeRef_AddressType)
      ->setStreetLines($recipient_address['address_line1'])
      ->setCity($recipient_address['locality'])
      ->setPostalCode($recipient_address['postal_code'])
      ->setCountryCode($recipient_address['country_code'])
      ->setCountryName($country_list[$recipient_address['country_code']]->render());

    if ($recipient_address['address_line2']) {
      $docTypeRef_AddressType->setStreetLines2($recipient_address['address_line2']);
    }

    return $docTypeRef_AddressType;
  }

}
