<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\RequestFactory;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RequestedShipmentTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ProcessShipmentRequestType;

/**
 * DocTypeRef_ProcessShipmentRequestType factory.
 */
final class DocTypeRef_ProcessShipmentRequestTypeFactory {

  /**
   * Constructs a new DocTypeRef_ProcessShipmentRequestType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ProcessShipmentRequestType
   *   The DocTypeRef_ProcessShipmentRequestType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_ProcessShipmentRequestType {
    return (new DocTypeRef_ProcessShipmentRequestType)
      ->setRequest(RequestFactory::createFromShipment($shipment))
      ->setRequestedShipment(DocTypeRef_RequestedShipmentTypeFactory::createFromShipment($shipment));
  }

}
