<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\EnumType\Content;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_CommoditiesTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_InternationDetailType;

/**
 * DocTypeRef_InternationDetailType Factory.
 */
final class DocTypeRef_InternationDetailTypeFactory {

  /**
   * Constructs a new DocTypeRef_ContactInfoType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_InternationDetailType
   *   The DocTypeRef_InternationDetailType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_InternationDetailType {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugIn()->getConfiguration();
    $shipper_address = $shipping_method_config['api_information']['requested_shipment']['ship']['shipper']['address'];
    $recipient_address = $shipment->getShippingProfile()->get('address')->first()->getValue();
    $is_domestic = $shipper_address['country_code'] === $recipient_address['country_code'];
    /** @var \Drupal\commerce_dhl_express\Plugin\Commerce\ShippingMethod\DhlExpressInterface $shipping_method_plugin */
    $shipping_method_plugin = $shipment->getShippingMethod()->getPlugIn();
    $is_eu = in_array($recipient_address['country_code'], $shipping_method_plugin::EU_COUNTRY_CODES);

    return (new DocTypeRef_InternationDetailType)
      ->setCommodities(DocTypeRef_CommoditiesTypeFactory::createFromShipment($shipment))
      ->setContent($is_domestic || $is_eu ? Content::VALUE_DOCUMENTS : Content::VALUE_NON_DOCUMENTS);
  }

}
