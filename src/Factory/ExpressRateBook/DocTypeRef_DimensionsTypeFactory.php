<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\physical\LengthUnit;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_DimensionsType;

/**
 * DocTypeRef_DimensionsType Factory.
 */
final class DocTypeRef_DimensionsTypeFactory {

  /**
   * Constructs a new DocTypeRef_ContactInfoType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_DimensionsType
   *   The DocTypeRef_DimensionsType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_DimensionsType {
    $package = $shipment->getPackageType();

    return (new DocTypeRef_DimensionsType)
      ->setLength((float) $package->getLength()->convert(LengthUnit::METER)->getNumber())
      ->setWidth((float) $package->getWidth()->convert(LengthUnit::METER)->getNumber())
      ->setHeight((float) $package->getHeight()->convert(LengthUnit::METER)->getNumber());
  }

}
