<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ContactTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactInfoType;

/**
 * DocTypeRef_ContactInfoType Factory.
 */
final class DocTypeRef_ContactInfoTypeFactory {

  /**
   * Constructs a new DocTypeRef_ContactInfoType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactInfoType
   *   The DocTypeRef_ContactInfoType instance.
   */
  public static function createShipperFromShipment(ShipmentInterface $shipment): DocTypeRef_ContactInfoType {
    return (new DocTypeRef_ContactInfoType)
      ->setContact(DocTypeRef_ContactTypeFactory::createShipperContactFromShipment($shipment))
      ->setAddress(DocTypeRef_AddressTypeFactory::createShipperAddressFromShipment($shipment));
  }

  /**
   * Constructs a new DocTypeRef_ContactInfoType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactInfoType
   *   The DocTypeRef_ContactInfoType instance.
   */
  public static function createRecipientFromShipment(ShipmentInterface $shipment): DocTypeRef_ContactInfoType {
    return (new DocTypeRef_ContactInfoType)
      ->setContact(DocTypeRef_ContactTypeFactory::createRecipientContactFromShipment($shipment))
      ->setAddress(DocTypeRef_AddressTypeFactory::createRecipientAddressFromShipment($shipment));
  }

}
