<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\physical\WeightUnit;
use Drupal\physical\Calculator;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_WeightType;

/**
 * DocTypeRef_WeightType Factory.
 */
final class DocTypeRef_WeightTypeFactory {

  /**
   * Constructs a new DocTypeRef_WeightType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_WeightType
   *   The DocTypeRef_WeightType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_WeightType {
    return (new DocTypeRef_WeightType)
      ->setValue((float) Calculator::round($shipment->getWeight()->convert(WeightUnit::KILOGRAM)->getNumber(), 3));
  }

}
