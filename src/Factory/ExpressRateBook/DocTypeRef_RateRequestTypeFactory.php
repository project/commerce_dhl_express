<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\RequestTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateRequestType;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RequestedShipmentType2Factory;

/**
 * DocTypeRef_RateRequestType factory.
 */
final class DocTypeRef_RateRequestTypeFactory {

  /**
   * Constructs a new DocTypeRef_RateRequestType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RateRequestType
   *   The DocTypeRef_RateRequestType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_RateRequestType {
    return (new DocTypeRef_RateRequestType)
      ->setRequest(RequestTypeFactory::createFromShipment($shipment))
      ->setRequestedShipment(DocTypeRef_RequestedShipmentType2Factory::createFromShipment($shipment));
  }

}
