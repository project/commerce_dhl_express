<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactType;

/**
 * DocTypeRef_ContactType factory.
 */
final class DocTypeRef_ContactTypeFactory {

  /**
   * Constructs a new DocTypeRef_ContactInfoType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactType
   *   The DocTypeRef_ContactType instance.
   */
  public static function createShipperContactFromShipment(ShipmentInterface $shipment): DocTypeRef_ContactType {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugIn()->getConfiguration();
    $shipper_contact_config = $shipping_method_config['api_information']['requested_shipment']['ship']['shipper']['contact'];

    return (new DocTypeRef_ContactType)
      ->setPersonName($shipper_contact_config['person_name'])
      ->setCompanyName($shipper_contact_config['company_name'])
      ->setPhoneNumber($shipper_contact_config['phone_number'])
      ->setEmailAddress($shipper_contact_config['email_address']);
  }

  /**
   * Constructs a new DocTypeRef_ContactInfoType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_ContactType
   *   The DocTypeRef_ContactType instance.
   */
  public static function createRecipientContactFromShipment(ShipmentInterface $shipment): DocTypeRef_ContactType {
    $recipient_address = $shipment->getShippingProfile()->get('address')->first()->getValue();
    $recipient_name = $recipient_address['given_name'] . ' ' . $recipient_address['family_name'];
    $recipient_company = $recipient_address['organization'];

    return (new DocTypeRef_ContactType)
      ->setPersonName($recipient_name)
      ->setCompanyName($recipient_company ? $recipient_company : $recipient_name)
      // There is no "native" phone field available;
      // add a subscriber to the event dispatched above to alter the recipient contact.
      ->setPhoneNumber('0000000000')
      ->setEmailAddress($shipment->getOrder()->getEmail());
  }

}
