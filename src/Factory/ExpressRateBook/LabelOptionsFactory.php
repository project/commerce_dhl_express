<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\LabelOptions;
use Maetva\DhlExpress\ExpressRateBook\EnumType\RequestLabelsToFitA4;

/**
 * LabelOptions Factory.
 */
final class LabelOptionsFactory {

  /**
   * Constructs a new LabelOptions instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\LabelOptions
   *   The LabelOptions instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): LabelOptions {
    return (new LabelOptions)
      ->setRequestLabelsToFitA4(RequestLabelsToFitA4::VALUE_Y);
  }

}
