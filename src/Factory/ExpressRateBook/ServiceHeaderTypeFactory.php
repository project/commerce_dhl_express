<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\ServiceHeaderType;

/**
 * ServiceHeaderType factory.
 */
final class ServiceHeaderTypeFactory {

  /**
   * Constructs a new ServiceHeaderType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\ServiceHeaderType
   *   The ServiceHeaderType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): ServiceHeaderType {
    /** @var \Drupal\commerce_dhl_express\Plugin\Commerce\ShippingMethod\DhlExpressInterface $shipping_method_plugin */
    $shipping_method_plugin = $shipment->getShippingMethod()->getPlugIn();

    return (new ServiceHeaderType)
      ->setMessageTime(
        \Drupal::service('date.formatter')->format(
          \Drupal::time()->getCurrentTime(),
          'custom',
          'Y-m-d\TH:i:sP'
        )
      )
      ->setMessageReference(bin2hex(openssl_random_pseudo_bytes(16)))
      ->setWebstorePlatform($shipping_method_plugin::WEBSTORE_PLATFORM)
      ->setShippingSystemPlatform($shipping_method_plugin::SHIPPING_SYSTEM_PLATFORM)
      ->setPlugIn($shipping_method_plugin::PLUG_IN);
  }

}
