<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\EnumType\Content;
use Maetva\DhlExpress\ExpressRateBook\EnumType\DropOffType2;
use Maetva\DhlExpress\ExpressRateBook\EnumType\PaymentInfo2;
use Maetva\DhlExpress\ExpressRateBook\EnumType\NextBusinessDay2;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ShipType2Factory;
use Maetva\DhlExpress\ExpressRateBook\EnumType\UnitOfMeasurement2;
use Maetva\DhlExpress\ExpressRateBook\EnumType\CustomerAgreementInd2;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_PackagesType2Factory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedShipmentType2;

/**
 * DocTypeRef_RequestedShipmentType2 Factory.
 */
final class DocTypeRef_RequestedShipmentType2Factory {

  /**
   * Constructs a new DocTypeRef_RequestedShipmentType2 instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedShipmentType2
   *   The DocTypeRef_RequestedShipmentType2 instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_RequestedShipmentType2 {
    $shipping_method_plugin = $shipment->getShippingMethod()->getPlugIn();
    $shipping_method_config = $shipping_method_plugin->getConfiguration();
    $shipper_address = $shipping_method_config['api_information']['requested_shipment']['ship']['shipper']['address'];
    $recipient_address = $shipment->getShippingProfile()->get('address')->first()->getValue();
    $is_domestic = $shipper_address['country_code'] === $recipient_address['country_code'];
    $is_eu = in_array($recipient_address['country_code'], $shipping_method_plugin::EU_COUNTRY_CODES);

    return (new DocTypeRef_RequestedShipmentType2)
      ->setNextBusinessDay(NextBusinessDay2::VALUE_Y)
      ->setServiceType($shipment->getShippingService())
      ->setCustomerAgreementInd(CustomerAgreementInd2::VALUE_N)
      ->setDropOffType(DropOffType2::VALUE_REGULAR_PICKUP)
      ->setShipTimestamp(
        \Drupal::service('date.formatter')->format(
          \Drupal::time()->getCurrentTime() + 60,
          'custom',
          'Y-m-d\TH:i:s\G\M\TP'
        )
      )
      ->setUnitOfMeasurement(UnitOfMeasurement2::VALUE_SI)
      ->setPaymentInfo(PaymentInfo2::VALUE_DAP)
      ->setAccount($shipping_method_config['api_information']['credentials']['account'])
      ->setShip(DocTypeRef_ShipType2Factory::createFromShipment($shipment))
      ->setPackages(DocTypeRef_PackagesType2Factory::createFromShipment($shipment))
      ->setContent($is_domestic || $is_eu ?  Content::VALUE_DOCUMENTS : Content::VALUE_NON_DOCUMENTS)
      ->setDeclaredValue((float) $shipment->getTotalDeclaredValue()->getNumber())
      ->setDeclaredValueCurrencyCode($shipment->getTotalDeclaredValue()->getCurrencyCode());
  }

}
