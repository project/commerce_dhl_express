<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_CommoditiesType;

/**
 * DocTypeRef_CommoditiesType Factory.
 */
final class DocTypeRef_CommoditiesTypeFactory {

  /**
   * Constructs a new DocTypeRef_CommoditiesType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_CommoditiesType
   *   The DocTypeRef_CommoditiesType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_CommoditiesType {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugIn()->getConfiguration();

    return (new DocTypeRef_CommoditiesType)
      ->setDescription($shipping_method_config['api_information']['requested_shipment']['international_detail']['commodities']['description'])
      ->setCustomsValue((float) $shipment->getTotalDeclaredValue()->getNumber());
  }

}
