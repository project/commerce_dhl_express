<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Maetva\DhlExpress\ExpressRateBook\EnumType\PaymentInfo;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_ShipTypeFactory;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedShipmentType;

/**
 * DocTypeRef_RequestedShipmentType Factory.
 */
final class DocTypeRef_RequestedShipmentTypeFactory {

  /**
   * Constructs a new DocTypeRef_RequestedShipmentType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_RequestedShipmentType
   *   The DocTypeRef_RequestedShipmentType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_RequestedShipmentType {
    return (new DocTypeRef_RequestedShipmentType)
      // We assume that the shipment is ready on request
      // so we set the timestamp to the current time.
      // However, as this value would be in the past once processed by the API,
      // it would not validate, so we arbitrary add 1 min.
      ->setShipTimestamp(
        \Drupal::service('date.formatter')->format(
          \Drupal::time()->getCurrentTime() + 60,
          'custom',
          'Y-m-d\TH:i:s\G\M\TP'
        )
      )
      ->setPaymentInfo(PaymentInfo::VALUE_DAP)
      ->setShipmentInfo(DocTypeRef_ShipmentInfoTypeFactory::createFromShipment($shipment))
      ->setInternationalDetail(DocTypeRef_InternationDetailTypeFactory::createFromShipment($shipment))
      ->setShip(DocTypeRef_ShipTypeFactory::createFromShipment($shipment))
      ->setPackages(DocTypeRef_PackagesTypeFactory::createFromShipment($shipment));
  }

}
