<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_PackagesType;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\DocTypeRef_RequestedPackagesTypeFactory;

/**
 * DocTypeRef_PackagesType Factory.
 */
final class DocTypeRef_PackagesTypeFactory {

  /**
   * Constructs a new DocTypeRef_PackagesType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\DocTypeRef_PackagesType
   *   The DocTypeRef_PackagesType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): DocTypeRef_PackagesType {
    return (new DocTypeRef_PackagesType)
      ->setRequestedPackages(DocTypeRef_RequestedPackagesTypeFactory::createFromShipment($shipment));
  }

}
