<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\ExpressRateBook;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\ExpressRateBook\StructType\RequestType;
use Drupal\commerce_dhl_express\Factory\ExpressRateBook\ServiceHeaderTypeFactory;

/**
 * RequestType factory.
 */
final class RequestTypeFactory {

  /**
   * Constructs a new RequestType instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\ExpressRateBook\StructType\RequestType
   *   The RequestType instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): RequestType {
    return (new RequestType)
      ->setServiceHeader(ServiceHeaderTypeFactory::createFromShipment($shipment));
  }

}
