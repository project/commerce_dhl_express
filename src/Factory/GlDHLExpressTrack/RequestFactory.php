<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\Request;

/**
 * Request factory.
 */
final class RequestFactory {

  /**
   * Constructs a new Request instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\GlDHLExpressTrack\StructType\Request
   *   The Request instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): Request {
    return (new Request)
      ->setServiceHeader(ServiceHeaderFactory::createFromShipment($shipment));
  }

}
