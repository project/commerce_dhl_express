<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\PubTrackingRequest;

/**
 * PubTrackingRequest factory.
 */
final class PubTrackingRequestFactory {

  /**
   * Constructs a new TrackingRequest instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\GlDHLExpressTrack\StructType\PubTrackingRequest
   *   The TrackingRequest instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): PubTrackingRequest {
    return (new PubTrackingRequest)
      ->setTrackingRequest(TrackingRequestFactory::createFromShipment($shipment));
  }

}
