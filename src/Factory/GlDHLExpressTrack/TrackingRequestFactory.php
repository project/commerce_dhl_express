<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\GlDHLExpressTrack\EnumType\LevelOfDetails;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackingRequest;

/**
 * TrackingRequest factory.
 */
final class TrackingRequestFactory {

  /**
   * Constructs a new TrackingRequest instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackingRequest
   *   The TrackingRequest instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): TrackingRequest {
    return (new TrackingRequest)
      ->setAWBNumber(ArrayOfAWBNumberFactory::createFromShipment($shipment))
      ->setLevelOfDetails(LevelOfDetails::VALUE_ALL_CHECKPOINTS)
      ->setRequest(RequestFactory::createFromShipment($shipment));
  }

}
