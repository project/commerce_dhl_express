<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\GlDHLExpressTrack\ArrayType\ArrayOfAWBNumber;

/**
 * ArrayOfAWBNumber factory.
 */
final class ArrayOfAWBNumberFactory {

  /**
   * Constructs a new ArrayOfAWBNumber instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\GlDHLExpressTrack\ArrayType\ArrayOfAWBNumber
   *   The ArrayOfAWBNumber instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): ArrayOfAWBNumber {
    return (new ArrayOfAWBNumber([/* '8066924740' */ $shipment->getTrackingCode()]));
  }

}
