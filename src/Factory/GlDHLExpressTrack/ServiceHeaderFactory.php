<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\ServiceHeader;

/**
 * ServiceHeader factory.
 */
final class ServiceHeaderFactory {

  /**
   * Constructs a new ServiceHeader instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\GlDHLExpressTrack\StructType\ServiceHeader
   *   The ServiceHeader instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): ServiceHeader {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugIn();

    return (new ServiceHeader)
      ->setMessageTime(
        \Drupal::service('date.formatter')->format(
          \Drupal::time()->getCurrentTime(),
          'custom',
          'Y-m-d\TH:i:sP'
        )
      )
      ->setMessageReference(bin2hex(openssl_random_pseudo_bytes(16)))
      ->setWebstorePlatform($shipping_method_config::WEBSTORE_PLATFORM)
      ->setShippingSystemPlatform($shipping_method_config::SHIPPING_SYSTEM_PLATFORM)
      ->setPlugIn($shipping_method_config::PLUG_IN);
  }

}
