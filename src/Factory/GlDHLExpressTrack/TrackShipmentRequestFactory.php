<?php

declare(strict_types = 1);

namespace Drupal\commerce_dhl_express\Factory\GlDHLExpressTrack;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackShipmentRequest;

/**
 * TrackShipmentRequest factory.
 */
final class TrackShipmentRequestFactory {

  /**
   * Constructs a new TrackShipmentRequest instance from a shipment entity.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\DhlExpress\GlDHLExpressTrack\StructType\TrackShipmentRequest
   *   The TrackShipmentRequest instance.
   */
  public static function createFromShipment(ShipmentInterface $shipment): TrackShipmentRequest {
    return  (new TrackShipmentRequest)
      ->setTrackingRequest(PubTrackingRequestFactory::createFromShipment($shipment));
  }

}
